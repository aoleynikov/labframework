using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using UnityEngine;

namespace lab.framework
{
    public class SystemsContainer
    {
        private readonly List<IInitializable> initializables = new List<IInitializable>();
        private readonly List<ILateInitializable> lateInitializables = new List<ILateInitializable>();
        private readonly List<ITickable> tickables = new List<ITickable>();
        private readonly List<ILateTickable> lateTickables = new List<ILateTickable>();
        private readonly List<IDisposable> disposables = new List<IDisposable>();

        private readonly DIResolver diResolver = new DIResolver();
       
        public void InitializeAll(params object[] arg)
        {
            foreach (var initializable in initializables)
            {
                initializable.Initialize(arg);
            }
        }
        
        public void LateInitializeAll()
        {
            foreach (var lateInitializable in lateInitializables)
            {
                lateInitializable.LateInitialize();
            }
        }

        public void UpdateAll()
        {
            foreach (var tickable in tickables)
            {
                tickable.Tick();
            }      
        }

        public void LateUpdateAll()
        {
            foreach (var lateTickable in lateTickables)
            {
                lateTickable.LateTick();
            } 
        }

        public void DisposeAll()
        {
            foreach (var disposable in disposables)
            {
                disposable.Dispose();
            }
        }

        public dynamic Register(Type type, List<Type> typesFilter = null) 
        {
            TypeInfo typeInfo = type.GetTypeInfo();
            List<Type> interfaces = typeInfo.GetInterfaces().ToList();
            
            if (interfaces.Contains(typeof(ISystem)))
            {
                dynamic result = Activator.CreateInstance(type);
                RegisterSystem(result as ISystem, typesFilter);
                
                diResolver.AddTypeToInstance(type, result);
                
                return result;
            }

            return null;
        }
        
        public dynamic Register<T>(Type type, List<Type> typesFilter = null) 
        {
            TypeInfo typeInfo = type.GetTypeInfo();
            List<Type> interfaces = typeInfo.GetInterfaces().ToList();
            
            dynamic result = Activator.CreateInstance(type);
            RegisterSystem(result as ISystem, typesFilter);
            
            diResolver.AddTypeToInstance(typeof(T), result);
                
            return result;
        }

        public void RegisterInstance(dynamic instance, List<Type> typesFilter = null)
        {
            Type type = instance.GetType();
            TypeInfo typeInfo = type.GetTypeInfo();
            List<Type> interfaces = typeInfo.GetInterfaces().ToList();

            if (interfaces.Contains(typeof(ISystem)))
            {
                RegisterSystem(instance, typesFilter);
                diResolver.AddTypeToInstance(instance.GetType(), instance);
            }
        }
        
        public void ResolveAll()
        {
            diResolver.ResolveAll();
        }
        
        private void RegisterSystem(ISystem system, List<Type> typesFilter = null)
        {           
            Type systemType = system.GetType();
            List<Type> interfaces = systemType.GetInterfaces().ToList();

            if (typesFilter == null)
            {
                if (interfaces.Contains(typeof(IInitializable)))
                {
                    initializables.Add((IInitializable) system);
                }
                
                if (interfaces.Contains(typeof(ILateInitializable)))
                {
                    lateInitializables.Add((ILateInitializable) system);
                }
                
                if (interfaces.Contains(typeof(ITickable)))
                {
                    tickables.Add((ITickable) system);
                }
                
                if (interfaces.Contains(typeof(ILateTickable)))
                {
                    lateTickables.Add((ILateTickable) system);
                }
                
                if (interfaces.Contains(typeof(IDisposable)))
                {
                    disposables.Add((IDisposable) system);
                }
            }
            else
            {
                if (interfaces.Contains(typeof(IInitializable)) && typesFilter.Contains(typeof(IInitializable)))
                {
                    initializables.Add((IInitializable) system);
                }
                
                if (interfaces.Contains(typeof(ILateInitializable)) && typesFilter.Contains(typeof(ILateInitializable)))
                {
                    lateInitializables.Add((ILateInitializable) system);
                }
                
                if (interfaces.Contains(typeof(ITickable)) && typesFilter.Contains(typeof(ITickable)))
                {
                    tickables.Add((ITickable) system);
                }
                
                if (interfaces.Contains(typeof(ILateTickable)) && typesFilter.Contains(typeof(ILateTickable)))
                {
                    lateTickables.Add((ILateTickable) system);
                }
                
                if (interfaces.Contains(typeof(IDisposable)) && typesFilter.Contains(typeof(IDisposable)))
                {
                    disposables.Add((IDisposable) system);
                }
            }
        }
        
        
    }
}