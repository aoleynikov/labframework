﻿namespace lab.framework
{
    public interface IDisposable: ISystem
    {
        void Dispose();
    }
}