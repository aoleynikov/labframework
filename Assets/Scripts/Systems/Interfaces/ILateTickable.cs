namespace lab.framework
{
    public interface ILateTickable: ISystem
    {
        void LateTick();
    }
}