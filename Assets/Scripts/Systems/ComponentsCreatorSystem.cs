using UnityEngine;

namespace lab.framework
{
    public class ComponentsCreatorSystem: ISystem
    {
        public void Create<T>(string name, ref T component) where T : Component
        {
            GameObject gameManager = new GameObject(name);
            component = gameManager.AddComponent<T>();
        }
    }
}