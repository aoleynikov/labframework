using UnityEngine;

namespace lab.framework
{
    public class BootstrapBase: MonoBehaviour
    {
        protected SystemsContainer systemsContainer;
        protected IEventsProvider eventsProvider;
        
        protected virtual void Awake()
        {
            systemsContainer = new SystemsContainer();
        }
        
        protected virtual void Update()
        {
            systemsContainer.UpdateAll();
        }

        protected virtual void LateUpdate()
        {
            systemsContainer.LateUpdateAll();
        }

        protected virtual void OnDestroy()
        {
            systemsContainer.DisposeAll();
        }
        
        protected AsyncProcessor CreateAsyncProcessor()
        {
            GameObject asyncGO = new GameObject("Async Processor");
            AsyncProcessor asyncProcessor = asyncGO.AddComponent<AsyncProcessor>();
            return asyncProcessor;
        }
    }
}