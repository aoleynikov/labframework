using UnityEngine;

namespace lab.framework
{
    public class AsyncProcessor : MonoBehaviour
    {
        private void OnDestroy()
        {
            StopAllCoroutines();
        }
    }
}