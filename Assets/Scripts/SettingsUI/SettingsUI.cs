﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace lab.framework
{
	public class SettingsUI : MonoBehaviour
	{
		public ScriptableObject configScriptableObject;

		public string fieldName;
		private FieldInfo myFieldInfo;
		private Selectable editableComponent;

		private object lastFieldValue;

		private void Awake()
		{
			Type myTypeA = configScriptableObject.GetType();
			myFieldInfo = myTypeA.GetField(fieldName);

			object fieldValue = myFieldInfo.GetValue(configScriptableObject);
			ReInitValues(fieldValue);
		}

		private void Update()
		{

			object fieldValue = myFieldInfo.GetValue(configScriptableObject);
			if (EventSystem.current != null && editableComponent != null
			                                && editableComponent.gameObject ==
			                                EventSystem.current.currentSelectedGameObject)
			{
				return;
			}

			if (fieldValue != null && lastFieldValue == null ||
			    fieldValue != null && fieldValue.ToString() != lastFieldValue.ToString())
			{
				lastFieldValue = fieldValue;
				ReInitValues(fieldValue);
			}
		}

		private void ReInitValues(object fieldValue)
		{
			InputField input = GetComponent<InputField>();
			if (input != null)
			{
				editableComponent = input;
				input.onEndEdit.RemoveListener(OnInputChanged);
				input.text = fieldValue.ToString();
				input.onEndEdit.AddListener(OnInputChanged);
			}

			Toggle toggle = GetComponent<Toggle>();
			if (toggle != null)
			{
				editableComponent = toggle;
				toggle.onValueChanged.RemoveListener(OnToggleChanged);
				toggle.isOn = (bool) fieldValue;
				toggle.onValueChanged.AddListener(OnToggleChanged);
			}

			Text text = GetComponent<Text>();
			if (text != null)
			{
				if (fieldValue is float)
					text.text = ((float) fieldValue).ToString("0.00");
				else if (fieldValue is double)
					text.text = ((double) fieldValue).ToString("0.00");
				else
					text.text = fieldValue.ToString();
			}

			Slider slider = GetComponent<Slider>();
			if (slider != null)
			{
				editableComponent = slider;
				slider.onValueChanged.RemoveListener(OnSliderChanged);
				slider.value = float.Parse(fieldValue.ToString());
				slider.onValueChanged.AddListener(OnSliderChanged);
			}

			Dropdown dropdown = GetComponent<Dropdown>();
			if (dropdown != null)
			{
				editableComponent = dropdown;
				dropdown.onValueChanged.RemoveListener(OnDropdownChanged);
				dropdown.value = (int) fieldValue;
				dropdown.onValueChanged.AddListener(OnDropdownChanged);
			}

		}

		private void OnInputChanged(string value)
		{
			if (myFieldInfo.FieldType == typeof(int))
			{
				myFieldInfo.SetValue(configScriptableObject, int.Parse(value));
				lastFieldValue = int.Parse(value);
			}
			else if (myFieldInfo.FieldType == typeof(float))
			{
				myFieldInfo.SetValue(configScriptableObject, float.Parse(value));
				lastFieldValue = float.Parse(value);
			}
			else if (myFieldInfo.FieldType == typeof(double))
			{
				myFieldInfo.SetValue(configScriptableObject, double.Parse(value));
				lastFieldValue = double.Parse(value);
			}
			else
			{
				myFieldInfo.SetValue(configScriptableObject, value);
				lastFieldValue = value;
			}
		}

		private void OnToggleChanged(bool value)
		{
			lastFieldValue = value;
			myFieldInfo.SetValue(configScriptableObject, value);
		}

		private void OnSliderChanged(float value)
		{

			if (myFieldInfo.FieldType == typeof(int))
			{
				myFieldInfo.SetValue(configScriptableObject, Mathf.FloorToInt(value));
				lastFieldValue = Mathf.FloorToInt(value);
			}
			else if (myFieldInfo.FieldType == typeof(double))
			{
				myFieldInfo.SetValue(configScriptableObject, (double) value);
				lastFieldValue = (double) value;
			}
			else
			{
				myFieldInfo.SetValue(configScriptableObject, value);
				lastFieldValue = value;
			}

			myFieldInfo.SetValue(configScriptableObject, lastFieldValue);
		}

		private void OnDropdownChanged(int value)
		{
			myFieldInfo.SetValue(configScriptableObject, value);
			lastFieldValue = value;
		}

		private void OnColorPickerChanged(Color value)
		{
			myFieldInfo.SetValue(configScriptableObject, value);
			lastFieldValue = value;
		}
	}
}
