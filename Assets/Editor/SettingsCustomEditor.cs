namespace lab.framework
{
    using UnityEngine;
    using System.Collections;
    using UnityEditor;

    #if ENABLE_CUSTOM_SAVE
    [CustomEditor(typeof(SaveableScriptableObject), true)]
    #endif
    public class SettingsCustomEditor : Editor
    {
        public override void OnInspectorGUI()
        {
            DrawDefaultInspector();

            EditorGUI.BeginDisabledGroup(serializedObject.isEditingMultipleObjects);
            if (GUILayout.Button("Save"))
            {
                ((SaveableScriptableObject) target).SaveSettings();
            }
            EditorGUI.EndDisabledGroup();
        }
    }
}
